import javafx.scene.control.TableView;

import java.io.*;
import java.util.*;

public class Results implements Runnable {
    static QueueSimulator myQueueSim = new QueueSimulator();
    private ArrayList<String> showQueues = new ArrayList<String>();

    public ArrayList<String> getShowQueues() {
        return showQueues;
    }

    public void updateQueues(ArrayList<QueueManagement> c) {
        for(int i = 0; i < myQueueSim.getQ(); i++)
            showQueues.set(i, c.get(i).toString());
    }

    public void run() {
        PrintStream out = null;
        try {
            out = new PrintStream(new FileOutputStream(QueueSimulator.output));
        } catch (FileNotFoundException exc) {
            exc.printStackTrace();
        }
        System.setOut(out);

        myQueueSim.timeSimulationMAX = QueueSimulator.getTimeSimulationMAX();
        myQueueSim.currentTime = QueueSimulator.getCurrentTime();
        while(myQueueSim.currentTime < myQueueSim.timeSimulationMAX) {
            try {
                Thread.sleep(500); // waits until it is sure the Queue Simulation is done
            }
            catch (InterruptedException e1) {
                e1.printStackTrace();
            }
            updateQueues(QueueSimulator.getTotalQueues());
        }

        out.println(QueueSimulator.getLog().toString());
        out.println("Average waiting time: " + myQueueSim.calculateAverageWaitingTime());
    }
}



