import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.*;

public class QueueManagement implements Runnable {
    private ArrayDeque<Client> clientQueue;
    private int currentServiceTime;
    private int emptyTime = 0;//tracks how long the queue has been empty during simulation

    Comparator<Client> clientComparator = new ArrivalTimeComparation();//needed for sorting the clients based on their arrival time

    public QueueManagement() {
        clientQueue = new ArrayDeque<Client>();
        currentServiceTime = 0;
    }

    public ArrayDeque<Client> getClientQueue() {
        return clientQueue;
    }

    //compute sum of processing the client service time for choosing the queue with the minimum waiting time, so that clients will be
    //added to the right queue
    public void updateCurrentServiceTime() {
        int serviceTimeSum = 0;
        for(Client elem: clientQueue)
            serviceTimeSum += elem.getTimeService();

        currentServiceTime = serviceTimeSum;
    }

    //operations on the queue
    public void addClient(Client givenClient) {
        clientQueue.addLast(givenClient);
        updateCurrentServiceTime();
    }

    public void removeClient() {
        clientQueue.removeFirst();
        updateCurrentServiceTime();
    }

    public int size() {
        return clientQueue.size();
    }

    public boolean isEmpty() {
        return clientQueue.isEmpty();
    }
    public Client getFirst() {
        return clientQueue.getFirst();
    }
    public void removeFirst() {
        clientQueue.removeFirst();
    }

    public int getServiceTime() {
        return currentServiceTime;
    }

    public int getEmptyTime() {
        return emptyTime;
    }

    public void setEmptyTime(int emptyTime) {
        this.emptyTime = emptyTime;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer("");
        for(Client elem: clientQueue) {
            sb.append(elem.toString() + "\\");
        }
        System.out.println(sb.toString());
        return sb.toString();
    }

    @Override
    public void run() {
        emptyTime = 0;

        while(QueueSimulator.currentTime < QueueSimulator.timeSimulationMAX) {
            if(!clientQueue.isEmpty()) {
                Client currentClient = clientQueue.getFirst();
                //increase the total waiting time on this queue
                //waitTime[QueueSimulator.min] += (QueueSimulator.currentTime - currentClient.getTimeArrival());
                QueueSimulator.waitingTimeQueues[QueueSimulator.min] += (QueueSimulator.currentTime - currentClient.getTimeArrival());
                int decrementTime = currentClient.getTimeService() ; // check how much time this client requires in order to be processed

                while(decrementTime > 1) {
                    decrementTime--;
                    currentServiceTime--; //the total time decrements too
                    try { // waiting for a second
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                //time to process this client has passed => removal from queue
                clientQueue.removeFirst();
            }
            else {
                emptyTime++;
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        //QueueSimulator.setWaitingTimeQueues(waitTime);
    }


    //sort the clients based on arrival time while removing them one by one from the queue and then adding them back
    //one after the other in the corresponding order of their arrival time
    void sort() {
        ArrayList<Client> sortedClientArray = new ArrayList<Client>();

        while(!clientQueue.isEmpty()) {
            for(Client currentClient: clientQueue) {
                sortedClientArray.add(currentClient);
                clientQueue.removeFirst();
            }
        }
        sortedClientArray.sort(clientComparator);
        while(!sortedClientArray.isEmpty()) {
            Client currentClient = sortedClientArray.get(0);
            clientQueue.addLast(currentClient);
            sortedClientArray.remove(0);
        }
    }

    class ArrivalTimeComparation implements Comparator<Client> {

        public int compare(Client givenClient1, Client givenClient2) {
            if(givenClient1.getTimeArrival() > givenClient2.getTimeArrival())
                return 1;
            if(givenClient1.getTimeArrival() < givenClient2.getTimeArrival())
                return -1;

            return 0;
        }
    }
}

