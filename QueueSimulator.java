import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.concurrent.*;

public class QueueSimulator implements Runnable {
    public static ArrayList<QueueManagement> totalQueues = new ArrayList<QueueManagement>();
    QueueManagement waitingClients = new QueueManagement();//collection of waiting clients randomly generated
    private int N;
    private int Q;
    private int timeArrivalMIN;
    private int timeArrivalMAX;
    private int timeServiceMIN;
    private int timeServiceMAX;
    public static int timeSimulationMAX;
    public static int currentTime;
    public static int[] serviceTimesQueues = new int[20]; //waiting times for every queue
    public static volatile int[] waitingTimeQueues = new int[20]; //sums of waiting times for each queue // when start processing - arrival time
    public static int[] queueSizes = new int[20]; //initial sizes of queues
    public static int[] copyQueueSizes = new int[20];//copy array of queueSizes, because they will be altered after the printing process
    static int min = 0;
    double[] avgWaiting = new double[20];
    StringBuffer avgWaitingStr = new StringBuffer();
    private static StringBuffer log = new StringBuffer("The simulation is at it follows:\n");
    int currentIndex = 1;//variable used for generating unique ids for the clients
    static String output;

    public QueueSimulator() {
    }

    public QueueSimulator(int givenTimeSimulationMAX, int givenQ, int givenTimeArrivalMIN, int givenTimeArrivalMAX, int givenTimeServiceMIN, int givenTimeServiceMAX) {
        totalQueues = new ArrayList<QueueManagement>();
        timeSimulationMAX = givenTimeSimulationMAX;
        this.Q = givenQ;
        this.timeArrivalMIN = givenTimeArrivalMIN;
        this.timeArrivalMAX = givenTimeArrivalMAX;
        this.timeServiceMIN = givenTimeServiceMIN;
        this.timeServiceMAX = givenTimeServiceMAX;
    }

    public int getN() {
        return N;
    }

    public int getTimeArrivalMIN() {
        return timeArrivalMIN;
    }

    public int getTimeArrivalMAX() {
        return timeArrivalMAX;
    }

    public int getTimeServiceMIN() {
        return timeServiceMIN;
    }

    public int getTimeServiceMAX() {
        return timeServiceMAX;
    }

    public static int getTimeSimulationMAX() {
        return timeSimulationMAX;
    }

    public int getQ() {
        return Q;
    }

    public static int getCurrentTime() {
        return currentTime;
    }

    public static ArrayList<QueueManagement> getTotalQueues() {
        return totalQueues;
    }

    public String getAvgWaiting() {
        return avgWaiting.toString();
    }

    public static StringBuffer getLog() {
        return log;
    }

    public static void appendLog(String s) {
        log.append(s);
    }

    private void addQueue(QueueManagement givenQueue) {
        totalQueues.add(givenQueue);
    }

    private Client generateRandomClient(String name) {
        Random random = new Random();
        int randomID = 0, randomTimeArrival = 0, randomTimeService = 0;

        randomID = currentIndex++;
        randomTimeArrival = random.nextInt(this.timeArrivalMAX) + this.timeArrivalMIN;
        randomTimeService = random.nextInt(this.timeServiceMAX) + this.timeServiceMIN;
        Client currentClient = new Client(randomID, randomTimeArrival, randomTimeService);

        return currentClient;
    }

    //returns index of the queue with the minimum waiting time so that the info can be then passed to the log
    //then, the optimal queue can be chosen for the waiting client
    public int queueMinWaitingTime() {
        int min = 0;

        for (QueueManagement elem : totalQueues) {
            if (elem.getServiceTime() < totalQueues.get(min).getServiceTime())
                min = totalQueues.indexOf(elem);
        }

        return min;
    }

    public void initialize(int queuesNr) {
        for(int i = 0; i < this.N; i++)
            this.waitingClients.addClient(generateRandomClient("" + i)); // here I create a wait list with clients that need to be processed
        this.waitingClients.sort(); //sorted based on arrival time
        for (int i = 0; i < this.Q; i++)
            addQueue(new QueueManagement());

        for (int i = 0; i < this.Q; i++) {  //initialization for this simulation
            serviceTimesQueues[i] = 0;
            waitingTimeQueues[i] = 0;
            queueSizes[i] = 0;
        }
    }

    public void showWaitingClients() {
        log.append("Waiting clients: ");
        for (Client elem: waitingClients.getClientQueue()) {
            if(elem == waitingClients.getClientQueue().getLast())
                log.append(elem.toString());
            else
                log.append(elem.toString() + ", ");
        }
        log.append("\n");
    }

    public void showQueueStatus() {
        int index = 0;
        for (QueueManagement elem : totalQueues) {
            if (elem.isEmpty())
                log.append("Queue " + index + ": closed;\n");
            else {
                log.append("Queue " + index + ": ");
                for (Client cl : elem.getClientQueue()) {
                    if (cl == elem.getClientQueue().getLast())
                        log.append(cl.toString());
                    else
                        log.append(cl.toString() + ", ");
                }
                log.append("\n");
            }
            index++;
        }
    }

    public void run() {
        int nrZeros = 0;
        initialize(this.Q);//initialize and start all queues
        for (QueueManagement elem : totalQueues)
            (new Thread(elem)).start();//one thread per queue
        for (currentTime = 0; currentTime <= timeSimulationMAX; currentTime++) {
            log.append("Time " + currentTime + "\n");
            if (!waitingClients.isEmpty()) { //there are still clients to be processed
                Client currentClient = waitingClients.getFirst(); //get the first client in the waiting line/queue
                while (currentClient.getTimeArrival() == currentTime && !waitingClients.isEmpty()) {
                    min = queueMinWaitingTime();
                    totalQueues.get(min).addClient(currentClient); //add  the currentlly waiting client to the corresponding queue(the one with min waiting time)
                    serviceTimesQueues[min] = serviceTimesQueues[min] + currentClient.getTimeService();//increase the total waiting time in this queue
                    queueSizes[min]++; //increase the nr of clients processed by this queue
                    waitingClients.removeFirst();//remove already processed client from the queue
                    if (!waitingClients.isEmpty())
                        currentClient = waitingClients.getFirst();
                }
                showWaitingClients();
                showQueueStatus();
                for(int i = 0; i < this.Q; i++)
                    copyQueueSizes[i] = queueSizes[i];
            }
            else {
                //there are still clients in the service queues, but the waiting list is empty
                nrZeros = 0; //for checking if there is still a non-empty service queue; suppose  still a non-empty queue
                for(int i = 0; i < this.Q; i++) {
                    if (queueSizes[i] == 0)
                        nrZeros++;
                    queueSizes[i]--;
                }
                if(nrZeros < this.Q)
                    showQueueStatus();
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException exc) {
                exc.printStackTrace();
            }
        }
    }

    public String calculateAverageWaitingTime() {
        double averageWaiting = 0;

        for (int i = 0; i < this.Q; i++) {
            if (copyQueueSizes[i] != 0)
                this.avgWaiting[i] = waitingTimeQueues[i] / copyQueueSizes[i];//total waiting time/nr of clients
            else
                this.avgWaiting[i] = 0;

            averageWaiting += this.avgWaiting[i];
        }
        averageWaiting = averageWaiting / this.Q;
        //averageWaiting = waitingTimeTotal / this.Q;
        this.avgWaitingStr.append(averageWaiting);//total average of waiting for the clients in all the queues

        return this.avgWaitingStr.toString();
    }
    public static void main(String[] args) {
        QueueSimulator myQueueSimulator = new QueueSimulator();

        if(args.length < 2)
            throw new IllegalArgumentException();
        else
            output = args[1];

        try {
            //File myFile = new File("In-Test.txt");
            File myFile = new File(args[0]);
            FileReader fr = new FileReader(myFile);
            BufferedReader br = new BufferedReader(fr);
            String currentLine;

            while ((currentLine = br.readLine()) != null) {
                //get nr of customers by checking for content of first line
                myQueueSimulator.N = Integer.parseInt(Files.readAllLines(Paths.get(args[0])).get(0));
                //get nr of queues by checking for content of second line
                myQueueSimulator.Q = Integer.parseInt(Files.readAllLines(Paths.get(args[0])).get(1));
                //get simulation interval by checking for content of third line
                timeSimulationMAX = Integer.parseInt(Files.readAllLines(Paths.get(args[0])).get(2));
                //get minimum and maximum arrival time
                String lineContent = Files.readAllLines(Paths.get(args[0])).get(3);
                //find index of "," inside the line
                int commaIndex = lineContent.indexOf(',');
                myQueueSimulator.timeArrivalMIN = Integer.parseInt(lineContent.substring(0, commaIndex));
                myQueueSimulator.timeArrivalMAX = Integer.parseInt(lineContent.substring(commaIndex + 1, lineContent.length()));
                //get minimum and maximum service time
                lineContent = Files.readAllLines(Paths.get(args[0])).get(4);
                commaIndex = lineContent.indexOf(',');
                myQueueSimulator.timeServiceMIN = Integer.parseInt(lineContent.substring(0, commaIndex));
                myQueueSimulator.timeServiceMAX = Integer.parseInt(lineContent.substring(commaIndex + 1, lineContent.length()));
            }

            fr.close();
            System.out.println(myQueueSimulator.N + " " + myQueueSimulator.Q + " " + timeSimulationMAX + " " + myQueueSimulator.timeArrivalMIN
                    + " " + myQueueSimulator.timeArrivalMAX + " " + myQueueSimulator.timeServiceMIN + " " + myQueueSimulator.timeServiceMAX);
        } catch (IOException exc) {
            exc.printStackTrace();
        }

        //set the new printing source to be the file in the argument
        PrintStream out = null;
        try {
            out = new PrintStream(new FileOutputStream(args[1]));
        } catch (FileNotFoundException exc) {
            exc.printStackTrace();
        }
        System.setOut(out);

        //print the queue evolution to the output file
        (new Thread(myQueueSimulator)).start();
        Results results = new Results();
        (new Thread(results)).start();
    }
}

